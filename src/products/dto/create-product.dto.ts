import { isNotEmpty, IsNotEmpty } from 'class-validator';

export class CreateProductDto {
    @IsNotEmpty()
    productName: string;

    @IsNotEmpty()
    productDescription: string;

    @IsNotEmpty()
    productQuantity: number;
}