import { IsIn, IsNotEmpty, IsOptional } from "class-validator";
import { ProductStatus } from "../product-status.enum";

export class GetProductsFilterDto {
    @IsOptional()
    @IsIn([ProductStatus.IN_STOCK, ProductStatus.OUT_OF_STOCK])
    status: ProductStatus;

    @IsOptional()
    @IsNotEmpty()
    search: string;
}