import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { GetProductsFilterDto } from './dto/get-products-filter.dto';
import { ProductStatus } from './product-status.enum';
import { Product } from './product.entity';
import { ProductRepository } from './product.repository';

@Injectable()
export class ProductsService {
    constructor(
        @InjectRepository(ProductRepository)
        private productRepository: ProductRepository
    ) {}

    async getProducts(filterDto: GetProductsFilterDto): Promise<Product[]> {
        return this.productRepository.getProducts(filterDto);
    }

    async getProductById(id: number): Promise<Product> {
        const found = await this.productRepository.findOne(id); //any interaction with the db is an async operation, we dont know when it will end so use sawait
    
        if(!found) {
            throw new NotFoundException(`Product with ID "${id}" not found`);
        }

        return found;
    }

    async createProduct(createProductDto: CreateProductDto) {
        return this.productRepository.createProduct(createProductDto);
    }

    async deleteProduct(id: number): Promise<void> {
        const result = await this.productRepository.delete(id);
        
        if(!result.affected) {
            throw new NotFoundException(`Product with ID "${id}" not found`);
        }
    }

    // async updateProductStatus(id: number, status: ProductStatus): Promise<Product> {
    //     const product = await this.getProductById(id);
    //     product.status = status;
    //     await product.save();
    //     return product;
    // }

    async buyProduct(id: number, bought: number): Promise<Product> {
        const product = await this.getProductById(id);
        
        if(bought>product.productQuantity){
            throw new BadRequestException('Not enough stocks')
        }else{
            product.productQuantity -= bought;
            if(product.productQuantity <= 0)  {
                product.status = ProductStatus.OUT_OF_STOCK;
                product.productQuantity = 0;
            }else{
                product.status = ProductStatus.IN_STOCK;
            }
        }

        await product.save();
        return product;
    }

    async restockProduct(id: number, restocked: number): Promise<Product> {
        const product = await this.getProductById(id);
        product.productQuantity += restocked;

        product.status = ProductStatus.IN_STOCK;
        
        await product.save();
        return product;
    }

}
