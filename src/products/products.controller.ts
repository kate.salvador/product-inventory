import { Body, Controller, Request, Delete, Get, Param, ParseIntPipe, Patch, Post, Query, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { GetProductsFilterDto } from './dto/get-products-filter.dto';
import { ProductStatusValidationPipe } from './pipes/product-status-validation.pipe';
import { ProductStatus } from './product-status.enum';
import { Product } from './product.entity';
import { ProductsService } from './products.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('products')
export class ProductsController {
    constructor(private productsService: ProductsService) {}

    @UseGuards(AuthGuard('jwt'))
    @Post('auth/login')
    async login(@Request() req) {
        return req.user;
    }

    @Get() //query is "?search=TEXT&status=IN_STOCK" after the path
    getProducts(@Query(ValidationPipe) filterDto: GetProductsFilterDto): Promise<Product[]> {
        return this.productsService.getProducts(filterDto);
    }

    @Get('/:id') //param is just string/num after the path
    getProductById(@Param('id', ParseIntPipe) id: number): Promise<Product> {
        return this.productsService.getProductById(id);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post()
    @UsePipes(ValidationPipe)
    createProduct(@Body() createProductDto: CreateProductDto): Promise<Product> {
        return this.productsService.createProduct(createProductDto);
    }

    @UseGuards(AuthGuard('jwt'))
    @Delete('/:id')
    deleteProduct(@Param('id', ParseIntPipe) id: number): Promise<void> {
        return this.productsService.deleteProduct(id);
    }

    // @UseGuards(AuthGuard('jwt'))
    // @Patch('/:id/status')
    // updateProductStatus(
    //     @Param('id', ParseIntPipe) id: number,
    //     @Body('status', ProductStatusValidationPipe) status: ProductStatus
    // ): Promise<Product> {
    //     return this.productsService.updateProductStatus(id, status);
    // }

    @UseGuards(AuthGuard('jwt'))
    @Patch('/:id/buy')
    buyProduct(
        @Param('id', ParseIntPipe) id: number,
        @Body('quantity', ParseIntPipe) quantity: number
    ): Promise<Product> {
        return this.productsService.buyProduct(id, quantity);
    }

    @UseGuards(AuthGuard('jwt'))
    @Patch('/:id/restock')
    restockProduct(
        @Param('id', ParseIntPipe) id: number,
        @Body('quantity', ParseIntPipe) quantity: number
    ): Promise<Product> {
        return this.productsService.restockProduct(id, quantity);
    }

}
