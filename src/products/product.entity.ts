import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { ProductStatus } from "./product-status.enum";

@Entity()
export class Product extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    productName: string;

    @Column()
    productDescription: string;

    @Column()
    productQuantity: number;

    @Column()
    status: ProductStatus;

    //can also specify enums like:
    // @Column({
    //     type: "enum",
    //     enum: UserRole,
    //     default: UserRole.GHOST
    // })

}