import { EntityRepository, Repository } from "typeorm";
import { CreateProductDto } from "./dto/create-product.dto";
import { GetProductsFilterDto } from "./dto/get-products-filter.dto";
import { ProductStatus } from "./product-status.enum";
import { Product } from "./product.entity";

@EntityRepository(Product) //this is where you define custom db logic other than the built in methods
export class ProductRepository extends Repository<Product> {
    async getProducts(filterDto: GetProductsFilterDto): Promise<Product[]> {
        const { status, search } = filterDto;  //query builder, when db operations are complex. example here is our filter conditions
        const query = this.createQueryBuilder('product'); //method of repo; interacts with Product TABLE
        
        if(status) {
            query.andWhere('product.status = :status', { status }); //where overwrites, andWhere combines
        }

        if(search) {
            query.andWhere('(product.productName LIKE :search OR product.productDescription LIKE :search)', { search: `%${search}%`}); //LIKE is like = but allows partial match
        }


        const products = await query.getMany();
        return products;
    }

    async createProduct(createProductDto: CreateProductDto): Promise<Product> {
        const { productName, productDescription, productQuantity } = createProductDto;

        const product = new Product();
        product.productName = productName;
        product.productDescription = productDescription;
        product.productQuantity = productQuantity;
        product.status = ProductStatus.IN_STOCK;
        await product.save();

        return product;
    }


}