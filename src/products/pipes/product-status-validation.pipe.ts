import { BadRequestException, PipeTransform } from "@nestjs/common";
import { ProductStatus } from "../product-status.enum";

export class ProductStatusValidationPipe implements PipeTransform {
    readonly allowedStatuses = [ //new class property
        ProductStatus.IN_STOCK,
        ProductStatus.OUT_OF_STOCK
    ]

    transform(value: any) { //required to implement a transform method
        value = value.toUpperCase();

        if(!this.isStatusValid(value)){
            throw new BadRequestException(`"${value}" is an invalid status`);
        }

        return value;
    }

    private isStatusValid(status: any) { //custom validation pipe
        const idx = this.allowedStatuses.indexOf(status); //returns -1 if status does not exist in allowed statuses array
        return idx !== -1;
    }
}