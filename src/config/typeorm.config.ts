import { TypeOrmModuleOptions } from "@nestjs/typeorm";

export const typeOrmConfig: TypeOrmModuleOptions = {   //this is an interface
    type: 'postgres', //based on this type, type orm is going to know wc driver to use
    host: 'localhost',
    port: 5432,
    username: 'postgres',
    password: 'password',
    database: 'productinventory',
    autoLoadEntities: true,
    synchronize: true,
    entities: []
};